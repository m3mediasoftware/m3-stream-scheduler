<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class M3_Stream_Scheduler_Smil {
	
	function m3ss_load_smils()
    {
    	global $wpdb;
    	$smils_table_name = $wpdb->prefix . 'm3ss_smils';
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';

		$sql = "SELECT sm.id, sm.name, sm.title, ap.name as application
				FROM $smils_table_name sm
				JOIN $application_table_name ap ON ap.id = sm.application_id";

    	$result = $mylink = $wpdb->get_results($sql, ARRAY_A );
    	
    	header('Content-type: application/json');
    	echo json_encode($result);
    	exit();
	}
    
    function m3ss_load_smil()
    {
    	global $wpdb;
    	$smils_table_name = $wpdb->prefix . 'm3ss_smils';
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';

		$sql = "SELECT sm.id, sm.name, sm.title, ap.name as application
				FROM $smils_table_name sm
				JOIN $application_table_name ap ON ap.id = sm.application_id 
                WHERE sm.id = '" . $_POST["id"] . "'";

    	$result = $mylink = $wpdb->get_row($sql, ARRAY_A );
    	
    	header('Content-type: application/json');
    	echo json_encode($result);
    	exit();
	}

    function m3ss_smil_create()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_smils';

    	$result = $wpdb->insert($application_table_name, array(
		    'name' => $_POST["name"],
		    'title' => $_POST["title"],
		    'application_id' => $_POST["application_id"],
		    'data' => $_POST["data"],
		));

		header('Content-type: application/json');

		if($result == 1)
		{
			echo json_encode(array(
				'success' => true
			));
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => "Application was not saved."
			));
		}
		exit();
    }
    
     function m3ss_smil_update()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_smils';

    	$result = $wpdb->update($application_table_name, array(
		    'id' => $_POST["id"]),
            array(
		    'name' => $_POST["name"],
		    'title' => $_POST["title"],
		    'application_id' => $_POST["application_id"],
		    'data' => $_POST["data"],
		));

		header('Content-type: application/json');

		if($result == 1)
		{
			echo json_encode(array(
				'success' => true
			));
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => "Application was not saved."
			));
		}
		exit();
    }
    
    function m3ss_smil_delete()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_smils';

    	$result = $wpdb->delete($application_table_name, array(
		    'id' => $_POST["id"],
		));

		header('Content-type: application/json');

		if($result == 1)
		{
			echo json_encode(array(
				'success' => true
			));
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => "Application was not deleted."
			));
		}
		exit();
    }

    function m3ss_smil_export()
    {
    	if(isset($_POST["id"]))
    	{
    		echo json_encode(array(
				'success' => false,
				'message' => "Wrong request."
			));
			exit();
    	}

    	global $wpdb;
    	$smils_table_name = $wpdb->prefix . 'm3ss_smils';
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';

		$sql = "SELECT sm.id, sm.name, sm.title, ap.name as application
				FROM $smils_table_name sm
				JOIN $application_table_name ap ON ap.id = sm.application_id";

    	$result = $mylink = $wpdb->get_results($sql, ARRAY_A );


    	$domain = get_option('m3ss_wowza_domain');
    	$port = get_option('m3ss_wowza_port');
    	$path = get_option('m3ss_wowza_path');

    	$url = "http://$domain:$port/$path";

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		$content = curl_exec($ch);
		/**
		$json = json_decode($content);
		if(json_last_error() != JSON_ERROR_NONE) {
			echo $content;
			exit();
		}
		if($json->status == "success" && isset($json->file_name)) {
			Recording::model()->updateRecordingFromJson($json);
		}
		*/
		echo $content;
		curl_close($ch);
    }
}