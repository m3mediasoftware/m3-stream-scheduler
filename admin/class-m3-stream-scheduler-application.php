<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class M3_Stream_Scheduler_Application {

	function m3ss_save_application()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';
    	//$sql = "INSERT INTO `$application_table_name` (`name`, `type`) VALUES ($_POST["app-name"], $_POST["app-type"]);";
    	$result = $wpdb->insert($application_table_name, array(
		    'name' => $_POST["name"],
		    'type' => $_POST["type"]
		));

		header('Content-type: application/json');

		if($result == 1)
		{
			echo json_encode(array(
				'success' => true
			));
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => "Application was not saved."
			));
		}
		exit();
	}

	function m3ss_load_application()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';
    	$result = $mylink = $wpdb->get_results( "SELECT id, name, type FROM $application_table_name ORDER BY name", ARRAY_A );
    	
    	header('Content-type: application/json');
    	echo json_encode($result);
    	exit();
	}

	function m3ss_delete_applications()
    {
    	global $wpdb;
    	$application_table_name = $wpdb->prefix . 'm3ss_applications';

    	$ids = explode(",", $_POST["ids"]);
    	$deleted_rows = 0;
    	foreach ($ids as $id)
    	{
    		//$result = $mylink = $wpdb->get_results( "SELECT id, name, type FROM $application_table_name ORDER BY name", ARRAY_A );
    		$result = $wpdb->query("DELETE FROM $application_table_name WHERE id IN ($id);");
    		if($result >= 1) 
    		{
    			$deleted_rows++;
    		}
    	}
    	header('Content-type: application/json');
    	echo json_encode(array(
    		"deleted_rows" => $deleted_rows
		));
    	exit;
    }
}