jQuery(function () {
    
    jQuery("#save-btn").click(function () {
        jQuery("#save-btn").prop("disabled", true);
        jQuery.post("admin-ajax.php",
        {
            action: "m3ss_save_settings",
            domain: jQuery.trim(jQuery("#wowza_domain").val()),
            port: jQuery.trim(jQuery("#wowza_port").val()),
            path: jQuery.trim(jQuery("#wowza_path").val())
        },
        function (json) {
            jQuery("#save-btn").prop("disabled", false);
            if(json.success) {                
                new PNotify({
                    text: "Seetings have been saved.",
                    type: 'success'
                });
                return false;
            }
        }).fail(function () {
            jQuery("#save-btn").prop("disabled", false);
            new PNotify({
                text: "Could not sent request for applications.",
                type: 'error'
            });
        });
    });
});
