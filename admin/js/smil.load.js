jQuery(function () {

  /** 
  * load application function
  */
  jQuery.fn.loadSmils = function() {

    jQuery.post("admin-ajax.php", {
      action: "m3ss_load_smils"
    }, function (json) {

      jQuery('#the-list').html(""); 
      
      if(json.length == 0) {
        return false;
      }

      jQuery('#the-list').html(""); 

      var html = '';
      for(var i = 0; i < json.length; i++) {
          var obj = json[i];

          html += '<tr id="row-'
               + obj.id
               +'"><td>'
               + obj.name +'</td><td>'
               + obj.title
               + '</td><td>'
               + obj.application 
               + '</td><td>'
               + '<a href="javascript: jQuery.noop" data-value="' + obj.id + '" onclick="exportSmil(this)" ><i class="fa fa-upload"></i></a>&nbsp;'
               + '<a href="javascript: jQuery.noop" data-value="' + obj.id + '" onclick="editSmil(this)" ><i class="fa fa-edit"></i></a>&nbsp;'
               + '<a href="javascript: jQuery.noop" data-value="' + obj.id + '" onclick="deleteSmil(this)" class="delete-row"><i class="fa fa-trash-o"></i></a>'
               + '</td></tr>';
      }

      jQuery('#the-list').append(html); 

    });
  };
  jQuery.fn.loadSmils();

});

function deleteSmil(obj) {
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure you want to delete this?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
    })).get().on('pnotify.confirm', function(){
         obj = jQuery(obj);
          var id = obj.attr("data-value");
          jQuery.post("admin-ajax.php",
            {
              action: "m3ss_smil_delete",
              id: id
            },
            function (json) {
              if (json.success) {
                  new PNotify({
                      text: "Schedule has been deleted.",
                      type: 'success'
                  });
                  location.reload();
                } else {
                   new PNotify({
                      text: "There was an error deleting the schedule.",
                      type: 'error'
                  });
                }
            }
          );
    });
}

function editSmil(obj) {
  obj = jQuery(obj);
  var id = obj.attr("data-value");

  jQuery.post("admin-ajax.php",
    {
      action: "admin.php",
      id: id, 
      page: "m3ss_smil_scheduler_dashboard"
    },
    function (json) {
      console.info(json);
    }
  );
}

function exportSmil(obj) {
  obj = jQuery(obj);
  var id = obj.attr("data-value");

  jQuery.post("admin-ajax.php",
    {
      action: "m3ss_smil_export",
      id: id
    },
    function (json) {
      console.info(json);
    }
  );
}