const NO_OPTION = -1;
const DELETE_OPTION = "delete";

jQuery(function () {

  /** 
  * request for saving user data.
  */
  jQuery("#save-btn").click(function () {

  	jQuery("#save-btn").prop("disabled", false);

     jQuery.post("admin-ajax.php", {
        action: "m3ss_save_application",
        id: jQuery("#app-id").val(),
        name: jQuery("#app-name").val(),
        type: jQuery('select[id=app-type]').val()
     }, function (result) {
        if (result.success) {
           
          jQuery("#app-name").val("");
          jQuery.fn.loadApplications();          

          new PNotify({
              text: "Application has been saved.",
              type: 'success'
          });
        } else {
           jQuery("#save-btn").prop("disabled", false);

           new PNotify({
              text: "There was an error saving application.",
              type: 'error'
          });
        }
     }).fail(function () {
        jQuery("#save-btn").prop("disabled", false);

        new PNotify({
            text: "Request could not be sent.",
            type: 'error'
        });
     });

     return false;
  });

  /** 
  * check/uncheck all elements - select-all 
  */
  jQuery("#select-all").click(function() {
      jQuery('input:checkbox').not(this).prop('checked', this.checked);
  });

  /**
  * Delete elements.
  */
  jQuery("#apply-btn").click(function() {
    var option = jQuery('select[id=bulk-action-selector-top]').val();
    if(option == NO_OPTION) {
      new PNotify({
          text: "Select an option.",
          type: 'error'
      });
      return false;
    }
    if(option == DELETE_OPTION) {

      var selected = jQuery('.abc:checked').length;

      if(selected == 0) {
        new PNotify({
          text: "Select an element to delete.",
          type: 'error'
        });
        return false;
      }

      // select all values and append them with ",".
      var values = jQuery(".abc:checked").map(function () {
        return this.value;
      }).get().join(",");

      jQuery.post("admin-ajax.php", {
        action: "m3ss_delete_applications",
        ids: values
      }, function (result) {
        if (result.deleted_rows) {
           
          jQuery.fn.loadApplications();          

          new PNotify({
              text: result.deleted_rows + "of " + selected + " have been removed.",
              type: 'success'
          });
        } else {
           jQuery("#save-btn").prop("disabled", false);

           new PNotify({
              text: "There was an error deleting application.",
              type: 'error'
          });
        }
      });

    }
  });

  /** 
  * load application function
  */
  jQuery.fn.loadApplications = function() {

    jQuery.post("admin-ajax.php", {
      action: "m3ss_load_application"
    }, function (json) {

      jQuery('#the-list').html(""); 
      
      if(json.length == 0) {
        return false;
      }

      jQuery('#the-list').html(""); 

      var html = '';
      for(var i = 0; i < json.length; i++) {
          var obj = json[i];

          html += '<tr id="row-'
               + obj.id
               +'"><td><input type="checkbox" id="check-'
               + obj.id + '" class="abc" onclick="validateCheckSelection(this)" value="'
               + obj.id +'" />'
               + '</td><td>'
               + obj.name
               + '</td><td>'
               + obj.type
               + '</td></tr>';
      }

      jQuery('#the-list').append(html); 

    });
  };

  jQuery.fn.loadApplications();

});

/**
* Check is all checksbox are/are not selected and check/unckeck
* top checkbox.
*/
function validateCheckSelection(input) {
  if(jQuery('.abc:checked').length == jQuery('.abc').length) {
    jQuery('#select-all').prop('checked', true);
  } else {
    jQuery('#select-all').prop('checked', false);
  }
}

function selectRow(input) {
  
  var id = jQuery(input).attr("data-value");
  var row = jQuery("#row-" + id);
/**
  console.info(row.find('td:eq(0)').html())
  console.info(row.find('td:eq(1)').html())
  console.info(row.find('td:eq(2)').html())
  */
}