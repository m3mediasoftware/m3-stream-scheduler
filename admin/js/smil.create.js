const REQUIRED_FIELD = "Field is required.";
const NOT_SENT = "Form could not be sent.";

jQuery(function () {

    jQuery("#form-data").validate({
        focusInvalid: true,
        rules: {
            name: {
                required: true
            },
            title: {
                required: true
            },
            applications: {
                required: true
            }
        },
        messages: {
            name: {
                required: REQUIRED_FIELD	
            },
            title: {
                required: REQUIRED_FIELD
            },
            applications: {
                required: REQUIRED_FIELD
            },
	        highlight: function( label ) {
	            jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
	        },
	        success: function( label ) {
	            jQuery(label).closest('.form-group').removeClass('has-error');
	            label.remove();
	        },
	        errorPlacement: function( error, element ) {
	            var placement = element.closest('.input-group');
	            if (!placement.get(0)) {
	                placement = element;
	            }
	            if (error.text() !== '') {
	                placement.after(error);
	            }
	        }
	    }
    });

    jQuery.post(
        "admin-ajax.php",
        {
            action: "m3ss_load_application"
        },
        function (json) {
        if(json.length == 0) {
            jQuery("#save-btn").prop("disabled", false);
            new PNotify({
                text: "No applications found.",
                type: 'error'
            });
            return false;
        }
        jQuery.each(json, function(index, value) {
            if(value != "") {
                jQuery('#applications').append(jQuery('<option>', {
                    value: value.id ,
                    text: value.name
                }));
            }               
        });
        
    }).fail(function () {
        jQuery("#save-btn").prop("disabled", false);
        new PNotify({
            text: "Could not sent request for applications.",
            type: 'error'
        });
    });

    jQuery("#cancel-btn").click(function () {
        window.location = goBackURL;
    });

    // request for saving user data.
    jQuery("#save-btn").click(function () {

       
    });

    var $w4finish = jQuery('#w4').find('ul.pager li.finish'),
        $w4validator = jQuery("#w4 form").validate({
        highlight: function(element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
            jQuery(element).remove();
        },
        errorPlacement: function( error, element ) {
            element.parent().append( error );
        }
    });

    $w4finish.on('click', function( ev ) {
        ev.preventDefault();
        
         if (!jQuery("#form-data").valid()) {
            return false;
        }

        var streams = jQuery("#streams").val();

        if(streams == null || streams == "") {
            new PNotify({
                text: "Streams field is required.",
                type: 'error'
            });
            return false;
        }

        var fistRow = jQuery('#video-tbody tr')[0];
        var cell = jQuery(fistRow).find('td:eq(0)');
        if(cell.hasClass('dataTables_empty')) {
            new PNotify({
                text: "Please, add at least one video.",
                type: 'error'
            });
            return false;
        }
        var hasUnsavedChanges = false;
        jQuery('#video-tbody tr').each(function() {

            var video = jQuery(this).find('td:eq(0)').html();

            if(jQuery(video).is("select")) {
                hasUnsavedChanges = true;
            } 
        });
        if(hasUnsavedChanges == true)                 {
            new PNotify({
                text: "Please, save your changes before continue.",
                type: 'error'
            });
            return false;
        }

        var json = generateJSON(streams);

        jQuery("#save-btn").prop("disabled", true);

        jQuery.post("admin-ajax.php", {
            action: "m3ss_smil_create",
            name: jQuery("#name").val(),
            title: jQuery("#title").val(),
            application_id: jQuery('select[id=applications]').val(),
            data: json
        }, function (result) {
            if (result.success) {
                new PNotify({
                    text: "great!!!",
                    type: 'success'
                });
                jQuery("#save-btn").prop("disabled", false);
            }
            else {
                jQuery("#save-btn").prop("disabled", false);
                new PNotify({
                    text: result.error,
                    type: 'error'
                });
            }
        }).fail(function () {
            jQuery("#save-btn").prop("disabled", false);
            new PNotify({
                text: NOT_SENT,
                type: 'error'
            });
        });

        return false;

    });

    jQuery('#w4').bootstrapWizard({
        tabClass: 'wizard-steps',
        nextSelector: 'ul.pager li.next',
        previousSelector: 'ul.pager li.previous',
        firstSelector: null,
        lastSelector: null,
        onNext: function( tab, navigation, index, newindex ) {
            //var validated = jQuery('#w4 form').valid();
            if(index == 1 && !jQuery("#form-data").valid()) {
                return false;
            }

            if(index == 2) {

                var fistRow = jQuery('#playlist-tbody tr')[0];
                var cell = jQuery(fistRow).find('td:eq(0)');
                if(cell.hasClass('dataTables_empty')) {
                    new PNotify({
                        text: "Please, add at least one playlist.",
                        type: 'error'
                    });
                    return false;
                }
                var hasUnsavedChanges = false;
                jQuery('#playlist-tbody tr').each(function() {

                    var plname = jQuery(this).find('td:eq(0)').html();

                    if(jQuery(plname).is("input")) {
                        hasUnsavedChanges = true;
                    } 
                });
                if(hasUnsavedChanges == true)                 {
                    new PNotify({
                        text: "Please, save your changes before continue.",
                        type: 'error'
                    });
                    return false;
                }
            }
        },
        onTabClick: function( tab, navigation, index, newindex ) {
            if ( newindex == index + 1 ) {
                return this.onNext( tab, navigation, index, newindex);
            } else if ( newindex > index + 1 ) {
                return false;
            } else {
                return true;
            }
        },
        onTabChange: function( tab, navigation, index, newindex ) {
            var $total = navigation.find('li').size() - 1;
            $w4finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
            jQuery('#w4').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
        },
        onTabShow: function( tab, navigation, index ) {
            var $total = navigation.find('li').length - 1;
            var $current = index;
            var $percent = Math.floor(( $current / $total ) * 100);
            jQuery('#w4').find('.progress-indicator').css({ 'width': $percent + '%' });
            tab.prevAll().addClass('completed');
            tab.nextAll().removeClass('completed');
        }
    });
});

function generateJSON(streams) {
    var json = '{ "streams": [';
    var streamList = streams.split(',');
    var placeCommaAtStart = false;

    streamList.forEach(function(entry) {
        if(!placeCommaAtStart) {
            placeCommaAtStart = true;
        } else {
            json += ',';
        }

        json += '"' + entry + '"'; 
    });

    placeCommaAtStart = false;

    json += '],';

    json += '"playlists": ['

    jQuery('#playlist-tbody tr').each(function() {

        if(!placeCommaAtStart) {
            placeCommaAtStart = true;
        } else {
            json += ',';
        }

        var plname = jQuery(this).find('td:eq(0)').html();
        var plstream = jQuery(this).find('td:eq(1)').html();
        var repeat = jQuery(this).find('td:eq(2)').html();
        var date = jQuery(this).find('td:eq(3)').html();
        var time = jQuery(this).find('td:eq(4)').html();

        json += ('{"name":' + '"' + plname + '",');
        json += ('"stream":' + '"' + plstream + '",');
        json += ('"repeat":' + repeat + ',');
        json += ('"date":' + '"' + date + '",');
        json += ('"time":' + '"' + time + '"}');
        
    });

    json += '],';

    json += '"videos": [';

    placeCommaAtStart = false;

    jQuery('#video-tbody tr').each(function() {

        if(!placeCommaAtStart) {
            placeCommaAtStart = true;
        } else {
            json += ',';
        }

        var streamname = jQuery(this).find('td:eq(0)').html();
        var start = jQuery(this).find('td:eq(1)').html();
        var length = jQuery(this).find('td:eq(2)').html();
        var playListForVideo = jQuery(this).find('td:eq(3)').html();

        json += ('{"streamname":' + '"' + streamname + '",');
        json += ('"start":' + '"' + start + '",');
        json += ('"length":' + length + ',');
        json += ('"playlist":' + '"' + playListForVideo + '"}');
        
    });

    json += ']}';

    return json;
}