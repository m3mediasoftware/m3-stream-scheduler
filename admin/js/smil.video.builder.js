/*
Name: 			Tables / Editable - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.1
*/

(function( $ ) {

	'use strict';

	var EditableTable2 = {

		options: {
			addButton: '#addToTable2',
			table: '#datatable-editable2',
			dialog: {
				wrapper: '#dialog2',
				cancelButton: '#dialogCancel2',
				confirmButton: '#dialogConfirm2',
			}
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {

			this.$table				= $( this.options.table );
			this.$addButton			= $( this.options.addButton );

			// dialog
			this.dialog				= {};
			this.dialog.$wrapper	= $( this.options.dialog.wrapper );
			this.dialog.$cancel		= $( this.options.dialog.cancelButton );
			this.dialog.$confirm	= $( this.options.dialog.confirmButton );

			return this;
		},

		build: function() {
			this.datatable = this.$table.DataTable({
				aoColumns: [
					null,
					null,
					null,
					null,
					{ "bSortable": false }
				],
				bFilter: false,
				bInfo: false,
				bPaginate: false
			});

			window.dt = this.datatable;

			return this;
		},

		events: function() {
			var _self = this;

			this.$table
				.on('click', 'a.save-row', function( e ) {
					e.preventDefault();

					_self.rowSave( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.cancel-row', function( e ) {
					e.preventDefault();

					_self.rowCancel( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.edit-row', function( e ) {
					e.preventDefault();

					_self.rowEdit( $(this).closest( 'tr' ) );
				})
				.on( 'click', 'a.remove-row', function( e ) {
					e.preventDefault();

					var $row = $(this).closest( 'tr' );

					$.magnificPopup.open({
						items: {
							src: _self.options.dialog.wrapper,
							type: 'inline'
						},
						preloader: false,
						modal: true,
						callbacks: {
							change: function() {
								_self.dialog.$confirm.on( 'click', function( e ) {
									e.preventDefault();

									_self.rowRemove( $row );
									$.magnificPopup.close();
								});
							},
							close: function() {
								_self.dialog.$confirm.off( 'click' );
							}
						}
					});
				});

			this.$addButton.on( 'click', function(e) {
				e.preventDefault();

				var td = $("#playlist-tbody").find("tr").find("td");

				if(td.hasClass("dataTables_empty")) {
					new PNotify({
	                    text: "Please add at least one playlist in order to add a video.",
	                    type: 'error'
	                });
					return false;
				}

				_self.rowAdd();
			});

			this.dialog.$cancel.on( 'click', function( e ) {
				e.preventDefault();
				$.magnificPopup.close();
			});

			return this;
		},

		// ==========================================================================================
		// ROW FUNCTIONS
		// ==========================================================================================
		rowAdd: function() {
			this.$addButton.attr({ 'disabled': 'disabled' });

			var actions,
				data,
				$row;

			actions = [
				'<a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>',
				'<a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>',
				'<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>',
				'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
			].join(' ');

			data = this.datatable.row.add([ '', '', '', '', actions ]);
			$row = this.datatable.row( data[0] ).nodes().to$();

			$row
				.addClass( 'adding' )
				.find( 'td:last' )
				.addClass( 'actions' );

			this.rowEdit( $row );

			this.datatable.order([0,'asc']).draw(); // always show fields
		},

		rowCancel: function( $row ) {
			var _self = this,
				$actions,
				i,
				data;

			if ( $row.hasClass('adding') ) {
				this.rowRemove( $row );
			} else {

				data = this.datatable.row( $row.get(0) ).data();
				this.datatable.row( $row.get(0) ).data( data );

				$actions = $row.find('td.actions');
				if ( $actions.get(0) ) {
					this.rowSetActionsDefault( $row );
				}

				this.datatable.draw();
			}
		},

		rowEdit: function( $row ) {
			var _self = this,
				data;

			data = this.datatable.row( $row.get(0) ).data();

			var usersStreams = $("#usersStreams").val();
			var streamList = usersStreams.split(',');

			var html = '<select class="form-control">';
			streamList.forEach(function(entry) {
			    html += '<option value="' + entry + '">';
			    html += entry;
			    html += '</option>';
			});
			html += '</select>';

			$row.find('td:eq(0)').html(html);
			$row.find('td:eq(0)').addClass("streams");

			$row.find('td:eq(1)').html( '<input type="text" class="form-control input-block" value="' + data[1] + '"/>' );
			$row.find('td:eq(1)').addClass("startVideo");

			$row.find('td:eq(2)').html( '<input type="text" class="form-control input-block" value="' + data[2] + '"/>' );
			$row.find('td:eq(2)').addClass("lengthVideo");

			

			html = '<select class="form-control">';

			$('#playlist-tbody tr').each(function() {
			    var cellText = $(this).find('td:eq(0)').html();

			    if( !(cellText.indexOf("<input") !== -1) ) {
			    	html += '<option value="' + cellText + '">';
				    html += cellText;
				    html += '</option>';
			    }
			    
			});

			html += '</select>';

			$row.find('td:eq(3)').html( html );
			$row.find('td:eq(3)').addClass("playList4Video");

			_self.rowSetActionsEditing( $row );
		},

		rowSave: function( $row ) {
			var _self     = this,
				$actions,
				values    = [];

			if ( $row.hasClass( 'adding' ) ) {
				this.$addButton.removeAttr( 'disabled' );
				$row.removeClass( 'adding' );
			}

			values = $row.find('td').map(function() {
				var $this = $(this);

				if ( $this.hasClass('actions') ) {
					_self.rowSetActionsDefault( $row );
					return _self.datatable.cell( this ).data();

				} else if ( $this.hasClass('streams') ) {
					return $.trim( $this.find('select option:selected').val());

				} else if ( $this.hasClass('startVideo') ) {

					var startValue = $this.find('input').val();

					if(!$.isNumeric(startValue)) {
						new PNotify({
		                    text: "Start field must be numeric. Also make sure there are not spaces on input.",
		                    type: 'error'
		                });
		                crashFlow.go();
					}
					return $.trim(startValue);
				} else if ( $this.hasClass('lengthVideo') ) {

					var lengthValue = $this.find('input').val();

					if(!$.isNumeric(lengthValue)) {
						new PNotify({
		                    text: "Length field must be numeric. Also make sure there are not spaces on input.",
		                    type: 'error'
		                });
		                crashFlow.go();
					}
					return $.trim(lengthValue);
				} else if ( $this.hasClass('playList4Video') ) {
					return $.trim( $this.find('select option:selected').val());
					
				} else {
					return $.trim( $this.find('input').val() );
				}
			});

			this.datatable.row( $row.get(0) ).data( values );

			$actions = $row.find('td.actions');
			if ( $actions.get(0) ) {
				this.rowSetActionsDefault( $row );
			}

			this.datatable.draw();
		},

		rowRemove: function( $row ) {
			if ( $row.hasClass('adding') ) {
				this.$addButton.removeAttr( 'disabled' );
			}

			this.datatable.row( $row.get(0) ).remove().draw();
		},

		rowSetActionsEditing: function( $row ) {
			$row.find( '.on-editing' ).removeClass( 'hidden' );
			$row.find( '.on-default' ).addClass( 'hidden' );
		},

		rowSetActionsDefault: function( $row ) {
			$row.find( '.on-editing' ).addClass( 'hidden' );
			$row.find( '.on-default' ).removeClass( 'hidden' );
		}

	};

	$(function() {
		EditableTable2.initialize();
	});

}).apply( this, [ jQuery ]);