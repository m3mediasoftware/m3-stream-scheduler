<div class="wrap nosubsub">
   <h1>Applications</h1>
   <div id="ajax-response"></div>
   <br class="clear" />
   <div id="col-container">
      <div id="col-right">
         <div class="col-wrap">
            <form id="posts-filter" method="post">
               <div class="tablenav top">
                  <div class="alignleft actions bulkactions">
                     <label for="bulk-action-selector-top" class="screen-reader-text">Select bulk action</label>
                     <select name="action" id="bulk-action-selector-top">
                        <option value="-1">Bulk Actions</option>
                        <option value="delete">Delete</option>
                     </select>
                     <input type="button" id="apply-btn" class="button action" value="Apply"  />
                  </div>
                  <div class='tablenav-pages one-page'><span class="displaying-num">1 item</span>
                     <span class='pagination-links'><span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>
                     <span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>
                     <span class="paging-input"><label for="current-page-selector" class="screen-reader-text">Current Page</label><input class='current-page' id='current-page-selector' type='text' name='paged' value='1' size='1' aria-describedby='table-paging' /> of <span class='total-pages'>1</span></span>
                     <span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>
                     <span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span></span>
                  </div>
                  <br class="clear" />
               </div>
               <h2 class='screen-reader-text'>Tags list</h2>
               <table class="wp-list-table widefat fixed striped tags">
                  <thead>
                     <tr>
                        <td  id='cb' class='manage-column column-cb check-column'><label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                           <input id="select-all" type="checkbox" />
                        </td>
                        <th scope="col" id='name' class='manage-column column-name column-primary'><span>Name</span></th>
                        <th scope="col" id='description' class='manage-column column-description'><span>Type</span></th>
                     </tr>
                  </thead>
                  <tbody id="the-list" data-wp-lists='list:applications'>
                  </tbody>
               </table>
               <br class="clear" />
            </form>
         </div>
      </div>
      <!-- /col-right -->
      <div id="col-left">
         <div class="col-wrap">
            <div class="form-wrap">
               <h2>Add New Application</h2>

               <input name="app-id" id="app-id" type="hidden" value="" />

               <div class="form-field form-required term-name-wrap">
                  <label for="app-name">Name</label>
                  <input name="app-name" id="app-name" type="text" value="" size="40" aria-required="true" />
                  <p>This name must match application name on Wowza Server.</p>
               </div>

               <div class="form-field term-slug-wrap">
                  <label for="app-type">Type</label>
                  <select name="app-type" id="app-type">
                     <option selected='selected' value='live'>Live</option>
                     <option value='vod'>VOD</option>
                  </select>
               </div>
               <p class="submit"><input type="button" name="save-btn" id="save-btn" class="button button-primary" value="Save"  /></p>
            </div>
         </div>
      </div>
      <!-- /col-left -->
   </div>
   <!-- /col-container -->
</div>
<!-- /wrap -->

