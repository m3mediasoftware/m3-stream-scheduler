<div class="col-xs-12">
   <form id="form-data">
      <section class="panel form-wizard" id="w4">
         <header class="panel-heading">
            <h2 class="panel-title">Create Playlist</h2>
         </header>
         <div class="panel-body">

            <div class="wizard-progress wizard-progress-lg">
               <div class="steps-progress">
                  <div class="progress-indicator"></div>
               </div>
               <ul class="wizard-steps">
                  <li class="active">
                     <a href="#w4-account" data-toggle="tab"><span>1</span>Playlist Info</a>
                  </li>
                  <li>
                     <a href="#w4-profile" data-toggle="tab"><span>2</span>Playlist Elements</a>
                  </li>
                  <li>
                     <a href="#w4-billing" data-toggle="tab"><span>3</span>Video Elements</a>
                  </li>
               </ul>
            </div>

            <div class="tab-content">
               <div id="w4-account" class="tab-pane active">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Name
                     </span>
                     </label>
                     <div class="input-group input-group-icon">
                        <input placeholder="Playlist Name" name="name" id="name" type="text" maxlength="255" class="form-control input-lg" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Title
                     </span>
                     </label>
                     <div class="input-group input-group-icon">
                        <input placeholder="Playlist Title" name="title" id="title" type="text" maxlength="255" class="form-control input-lg" />
                     </div>
                  </div>
                  <div class="form-group mb-lg">
                     <label>Aplication</label>
                     <div class="input-group input-group-icon">
                        <select id="applications" name="applications" class="form-control">
                           <option value="">Choose an application</option>
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Stream Names
                     </span>
                     </label>
                     <div class="input-group input-group-icon">
                        <!--<input placeholder="Playlist Title" name="streams" id="streams" type="text" maxlength="255" data-role="tagsinput" class="form-control input-lg" />-->
                        <input id="streams" name="streams" data-role="tagsinput" data-tag-class="label label-primary" class="form-control" />
                     </div>
                  </div>
               </div>

               <div id="w4-profile" class="tab-pane">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="mb-md">
                           <button id="addToTable" class="btn btn-primary">Add Playlist 
                           <i class="fa fa-plus"></i>
                           </button>
                        </div>
                     </div>
                  </div>
                  <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                     <thead>
                        <tr>
                           <th>Playlist Name</th>
                           <th>Play On Stream</th>
                           <th>Repeat</th>
                           <th>Schedule Date</th>
                           <th>Time</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody id="playlist-tbody"></tbody>
                  </table>
                  <br>
               </div>

               <div id="w4-billing" class="tab-pane">
                  <div class="row">
               <div class="col-sm-6">
                  <div class="mb-md">
                     <button id="addToTable2" class="btn btn-primary">Add Video 
                     <i class="fa fa-plus"></i>
                     </button>
                  </div>
               </div>
            </div>
            <input id="usersStreams" type="hidden" value="mystream,livestream,hugostream">
            <table class="table table-bordered table-striped mb-none" id="datatable-editable2">
               <thead>
                  <tr>
                     <th>Live Stream</th>
                     <th>Start</th>
                     <th>Length</th>
                     <th>Apply for Playlists</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody id="video-tbody"></tbody>
            </table>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <ul class="pager">
               <li class="previous disabled">
                  <a><i class="fa fa-angle-left"></i> Previous</a>
               </li>
               <li class="finish hidden pull-right">
                  <a>Finish</a>
               </li>
               <li class="next">
                  <a>Next <i class="fa fa-angle-right"></i></a>
               </li>
            </ul>
         </div>
      </section>
      <div id="dialog" class="modal-block mfp-hide">
         <section class="panel">
            <header class="panel-heading">
               <h2 class="panel-title">Are you sure?</h2>
            </header>
            <div class="panel-body">
               <div class="modal-wrapper">
                  <div class="modal-text">
                     <p>Are you sure that you want to delete this row?</p>
                  </div>
               </div>
            </div>
            <footer class="panel-footer">
               <div class="row">
                  <div class="col-md-12 text-right">
                     <button id="dialogConfirm" class="btn btn-primary">Confirm</button>
                     <button id="dialogCancel" class="btn btn-default">Cancel</button>
                  </div>
               </div>
            </footer>
         </section>
      </div>
      <div id="dialog2" class="modal-block mfp-hide">
         <section class="panel">
            <header class="panel-heading">
               <h2 class="panel-title">Are you sure?</h2>
            </header>
            <div class="panel-body">
               <div class="modal-wrapper">
                  <div class="modal-text">
                     <p>Are you sure that you want to delete this row?</p>
                  </div>
               </div>
            </div>
            <footer class="panel-footer">
               <div class="row">
                  <div class="col-md-12 text-right">
                     <button id="dialogConfirm2" class="btn btn-primary">Confirm</button>
                     <button id="dialogCancel2" class="btn btn-default">Cancel</button>
                  </div>
               </div>
            </footer>
         </section>
      </div>
   </form>
</div>