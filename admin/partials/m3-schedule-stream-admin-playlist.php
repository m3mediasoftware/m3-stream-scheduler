<div class="col-xs-12">
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
				<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
			</div>

			<h2 class="panel-title">Playlists</h2>
		</header>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-hover mb-none">
					<thead>
						<tr>
							<th>Name</th>
							<th>Title</th>
							<th>Application</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody id="the-list" data-wp-lists='list:smils'>
                  	</tbody>
				</table>
			</div>
		</div>
	</section>
</div>