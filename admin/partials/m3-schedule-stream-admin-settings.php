<div class="col-xs-12">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Settings</h2>
        </header>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="wowza_domain">Wowza server domain</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="wowza_domain" value="<?php echo esc_attr( get_option('m3ss_wowza_domain')); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="wowza_port">Wowza server port</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="wowza_port" value="<?php echo esc_attr( get_option('m3ss_wowza_port')); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="wowza_path">Wowza interface path</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="wowza_path" value="<?php echo esc_attr( get_option('m3ss_wowza_path')); ?>">
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                    <button id="save-btn" class="btn btn-primary">Sent</button>
                </div>
            </div>
        </footer>
    </section>
</div>