<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class M3_Stream_Scheduler_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function m3ss_add_menu()
	{
		add_menu_page(
	        	'Playlist',
	        	'Playlist',
	        	'manage_options',
	        	'm3_playlist',
	        	array(__CLASS__, 'm3_playlist_all'),
	            plugins_url('/images/scheduler_icon.png', __FILE__),
	            '2.2.9'
        );

        add_submenu_page(
        		'm3_playlist', 
        		'Scheduler', 
        		'Scheduler', 
        		'manage_options', 
        		'm3_stream_scheduler_dashboard', 
        		array(__CLASS__, 'm3_playlist_creator')
		);

        add_submenu_page(
        		'm3_playlist', 
        		'Applications', 
        		'Applications', 
        		'manage_options', 
        		'applications-dashboard', 
        		array(__CLASS__, 'm3ss_manage_applications')
		);

        add_submenu_page( 
        		'm3_playlist', 
        		'Analytify simple' . ' Settings', 
        		'<b style="color:#f9845b">Settings</b>', 
        		'manage_options',
            	'm3ss-settings', 
            	array(__CLASS__, 'm3ss_manage_settings'));
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{
		$dir = plugin_dir_url( __FILE__ );

		if(isset($_GET["page"]) && (substr($_GET["page"], 0, 3) === "m3_" || substr($_GET["page"], 0, 4) === "m3ss"))
		{
			$this->loadTemplateStyles($dir);
		} else {
			wp_enqueue_style( $this->plugin_name, $dir . 'css/plugin-name-admin.css', array(), $this->version, 'all' );
			wp_enqueue_style( "m3ss-pnotify-css", $dir . 'css/pnotify.custom.min.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		$dir = plugin_dir_url( __FILE__ );

		if(isset($_GET["page"]) && $_GET["page"] == "applications-dashboard")
		{
			wp_enqueue_script( "m3-applications-manager", $dir . 'js/applications-manage.js', array( 'jquery' ), $this->version, true );
		}

		if(isset($_GET["page"]) && (substr($_GET["page"], 0, 3) === "m3_" || substr($_GET["page"], 0, 4) === "m3ss"))
		{
			
			$this->loadTemplateScripts($dir);

			if($_GET["page"] === "m3_stream_scheduler_dashboard")
			{
				wp_enqueue_script("m3ss-smil-create-js", $dir . 'js/smil.create.js', array('jquery'), $this->version, true );
				wp_enqueue_script("m3ss-smil-playlist-builder-js", $dir . 'js/smil.playlist.builder.js', array('jquery'), $this->version, true );
				wp_enqueue_script("m3ss-smil-video-builder-js", $dir . 'js/smil.video.builder.js', array('jquery'), $this->version, true );
			}
			else if($_GET["page"] === "m3_playlist")
			{
				wp_enqueue_script("m3ss-smil-load-js", $dir . 'js/smil.load.js', array('jquery'), $this->version, true );
			}
			else if($_GET["page"] === "m3ss-settings")
			{
				wp_enqueue_script("m3ss-settings-js", $dir . 'js/settings.js', array('jquery'), $this->version, true );
			}

		} else {
			wp_enqueue_script( "m3ss-pnotify-js", $dir . 'js/pnotify.custom.min.js', array( 'jquery' ), $this->version, true );	
		}
	}

	function m3_playlist_all()
    { 
    	require_once plugin_dir_path( __FILE__ ) . 'partials/m3-schedule-stream-admin-playlist.php';
	}

	function m3_playlist_creator()
    { 
    	require_once plugin_dir_path( __FILE__ ) . 'partials/m3-schedule-stream-admin-playlist-creator.php';
	}

	function m3ss_manage_applications()
    {
    	require_once plugin_dir_path( __FILE__ ) . 'partials/m3-schedule-stream-admin-applications.php';
	}

	function m3ss_manage_settings()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/m3-schedule-stream-admin-settings.php';
	}

	function m3ss_save_settings()
    {
    	global $wpdb;
    	$table_name = $wpdb->prefix . 'options';

    	$wpdb->query("DELETE FROM $table_name WHERE option_name LIKE 'm3ss_%' ");

    	$result = $wpdb->insert($table_name, array(
		    'option_name' => 'm3ss_wowza_domain',
		    'option_value' => $_POST["domain"],
		    'autoload' => 'yes'
		));
		$result = $wpdb->insert($table_name, array(
		    'option_name' => 'm3ss_wowza_port',
		    'option_value' => $_POST["port"],
		    'autoload' => 'yes'
		));
		$result = $wpdb->insert($table_name, array(
		    'option_name' => 'm3ss_wowza_path',
		    'option_value' => $_POST["path"],
		    'autoload' => 'yes'
		));

		header('Content-type: application/json');

		echo json_encode(array(
			'success' => true,
			'message' => 'Settings have been saved.'
		));
		exit();
	}

    function loadTemplateStyles($dir)
    {
    	wp_enqueue_style( "m3ss-datatables-css", $dir . 'template/vendor/jquery-datatables-bs3/assets/css/datatables.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-font-awesome-css", $dir . 'template/vendor/font-awesome/css/font-awesome.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-magnific-popup-css", $dir . 'template/vendor/magnific-popup/magnific-popup.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-datepicker3-css", $dir . 'template/vendor/bootstrap-datepicker/css/datepicker3.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-bootstrap-timepicker-css", $dir . 'template/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-bootstrap-css", $dir . 'template/vendor/bootstrap/css/bootstrap.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-elusive-webfont-css", $dir . 'template/vendor/elusive-icons/css/elusive-webfont.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-bootstrap-tagsinput-css", $dir . 'template/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-theme-css", $dir . 'template/stylesheets/theme.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-default-css", $dir . 'template/stylesheets/skins/default.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-pnotify-css", $dir . 'template/vendor/pnotify/pnotify.custom.css', array(), $this->version, 'all' );
		wp_enqueue_style( "m3ss-theme-custom-css", $dir . 'template/stylesheets/theme-custom.css', array(), $this->version, 'all' );
    }

    function loadTemplateScripts($dir)
    {
    	wp_enqueue_script("m3ss-modernizr-js", $dir . 'template/vendor/modernizr/modernizr.js', array('jquery'), $this->version, false );

    	wp_enqueue_script("m3ss-bootstrap-js", $dir . 'template/vendor/bootstrap/js/bootstrap.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-nanoscroller-js", $dir . 'template/vendor/nanoscroller/nanoscroller.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-bootstrap-datepicker-js", $dir . 'template/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-magnific-popup-js", $dir . 'template/vendor/magnific-popup/magnific-popup.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-jquery.placeholder-js", $dir . 'template/vendor/jquery-placeholder/jquery.placeholder.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-pnotify.custom-js", $dir . 'template/vendor/pnotify/pnotify.custom.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-jquery-validate-js", $dir . 'template/vendor/jquery-validation/jquery.validate.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-bootstrap-tagsinput/bootstrap-tagsinput", $dir . 'template/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-theme-js", $dir . 'template/javascripts/theme.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-theme-custom-js", $dir . 'template/javascripts/theme.custom.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-theme-init-js", $dir . 'template/javascripts/theme.init.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-bootstrap-timepicker-js", $dir . 'template/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-select2-js", $dir . 'template/vendor/select2/select2.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-jquery-dataTables-js", $dir . 'template/vendor/jquery-datatables/media/js/jquery.dataTables.js', array('jquery'), $this->version, true );
		wp_enqueue_script("m3ss-dataTables-js", $dir . 'template/vendor/jquery-datatables-bs3/assets/js/datatables.js', array('jquery'), $this->version, true );

		wp_enqueue_script("m3ss-jquery-bootstrap-wizard", $dir . 'template/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js', array('jquery'), $this->version, true );
    }
}
