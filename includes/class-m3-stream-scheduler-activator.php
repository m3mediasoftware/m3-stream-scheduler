<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class M3_Stream_Scheduler_Activator {

	/**
	 * Add tables related to plugin.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();

		$application_table_name = $wpdb->prefix . 'm3ss_applications';
		$stream_table_name = $wpdb->prefix . 'm3ss_streams';
		$smil_table_name = $wpdb->prefix . 'm3ss_smils';

		$sql = "CREATE TABLE `$application_table_name` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`name` varchar(255) NOT NULL DEFAULT '',
		`type` varchar(255) NOT NULL DEFAULT '',
		PRIMARY KEY (`id`)
		) $charset_collate;

		CREATE TABLE `$stream_table_name` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`name` varchar(255) NOT NULL DEFAULT '',
		`status` varchar(100) NOT NULL DEFAULT 'inactive',
		`application_id` int(11) NOT NULL DEFAULT '0',
		PRIMARY KEY (`id`),
		KEY `fk_stream_application` (`application_id`),
		CONSTRAINT `fk_stream_application` FOREIGN KEY (`application_id`) REFERENCES `$application_table_name` (`id`)
		) $charset_collate;

		CREATE TABLE `$smil_table_name` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`name` varchar(255) NOT NULL DEFAULT '',
		`title` varchar(255) NOT NULL DEFAULT '',
		`application_id` int(11) NOT NULL DEFAULT '0',
		`data` text,
		PRIMARY KEY (`id`),
		KEY `application_id` (`application_id`),
		CONSTRAINT `fk_smil_application` FOREIGN KEY (`application_id`) REFERENCES `$application_table_name` (`id`)
		) $charset_collate;
		";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

}
