<?php
/**
*/

class WP_M3_Stream_Scheduler
{
	function __construct()
    {
    	add_action( 'admin_menu', array( $this, 'wpa_add_menu' ));
    	register_activation_hook( __FILE__, array( $this, 'wpa_install' ) );
        register_deactivation_hook( __FILE__, array( $this, 'wpa_uninstall' ) );
    }

    /*
      * Actions perform at loading of admin menu
      */
    function wpa_add_menu()
    {
        add_menu_page(
	        	'Scheduler',
	        	'Scheduler',
	        	'manage_options',
	        	'm3_stream_scheduler_dashboard',
	        	array(__CLASS__, 'wpa_page_file_path'),
	            plugins_url('images/scheduler_icon.png', __FILE__),
	            '2.2.9'
        );

        add_submenu_page(
        		'analytify-dashboard', 
        		'Analytify simple' . ' Dashboard', 
        		' Dashboard', 
        		'manage_options', 
        		'analytify-dashboard', 
        		array(__CLASS__, 'wpa_page_file_path')
		);

        add_submenu_page( 
        		'analytify-dashboard', 
        		'Analytify simple' . ' Settings', 
        		'<b style="color:#f9845b">Settings</b>', 
        		'manage_options', '
            	analytify-settings', 
            	array(__CLASS__, 'wpa_page_file_path'));
    }

    /*
     * Actions perform on activation of plugin
     */
    function wpa_install() {



    }

    /*
     * Actions perform on de-activation of plugin
     */
    function wpa_uninstall() {



    }

    function wpa_page_file_path()
    { 
    	echo("<div class='wrap'><h2>Opciones de mi sitio</h2></div>");
    	if(isset($_POST['action']) && $_POST['action'] == "salvaropciones")
    	{
	        update_option('miplugin_telefono',$_POST['telefono']);
	        update_option('miplugin_direccion',$_POST['direccion']);
	        update_option('miplugin_email',$_POST['email']);
	        echo("<div class='updated message' style='padding: 10px'>Opciones guardadas.</div>");
	    }
	?>
 
	    <form method='post'>
	        <input type='hidden' name='action' value='salvaropciones'> 
	        <table>
	            <tr>
	                <td>
	                    <label for='telefono'>Telefono</label>
	                </td>
	                <td>
	                    <input type='text' name='telefono' id='telefono' value='<?=get_option('miplugin_telefono')?>'>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <label for='direccion'>Dirección</label>
	                </td>
	                <td>
	                    <input type='text' name='direccion' id='direccion' value='<?=get_option('miplugin_direccion')?>'>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <label for='email'>Email</label>
	                </td>
	                <td>
	                    <input type='text' name='email' id='email' value='<?=get_option('miplugin_email')?>'>
	                </td>
	            </tr>
	            <tr>
	                <td colspan='2'>
	                    <input type='submit' value='Enviar'>
	                </td>
	            </tr>
	        </table>
	    </form>

    <?php
	}    
}

new WP_M3_Stream_Scheduler();

?>
