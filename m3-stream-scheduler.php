<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_Name
 *
 * @wordpress-plugin
 * Plugin Name:       M3 Stream Scheduler
 * Plugin URI:        https://bitbucket.org/m3mediasoftware/
 * Description:       Wordpress plugin to create stream playlist.
 * Version:           1.0.0
 * Author:            Hugo Mercado
 * Author URI:        https://bitbucket.org/hmercado
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       m3_stream_scheduler
 * Domain Path:       /en
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_m3_stream_scheduler() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-m3-stream-scheduler-activator.php';
	M3_Stream_Scheduler_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_m3_stream_scheduler() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-m3-stream-scheduler-deactivator.php';
	M3_Stream_Scheduler_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_m3_stream_scheduler' );
register_deactivation_hook( __FILE__, 'deactivate_m3_stream_scheduler' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-m3-stream-scheduler.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_m3_stream_scheduler() {

	$plugin = new M3_Stream_Scheduler();
	$plugin->run();

}
run_m3_stream_scheduler();
